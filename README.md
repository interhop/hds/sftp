# Usage

This deploys a sftp server behind a proxy server, for given users, method and folders.  

![Inspiration](https://i.stack.imgur.com/MgNQm.jpg)

Iptables redirects the sftp [as described here](https://serverfault.com/a/496865)


# Install

```
vagrant plugin install vagrant-hostmanager
git clone git@framagit.org:interhop/hds/sftp.git
git submodule update --init --recursive

# Optionally
pip3 install ansible~=5.2.0

```
